from __future__ import annotations

import logging
import os

import faust
from lorem import get_sentence
from prometheus_client import Counter, Histogram

from faust_prometheus.exporter import FaustPrometheusExporter

logger = logging.getLogger('app')

# Prometheus custom metrics
prometheus_counter_topic_1_messages = Counter(
    'topic_1_messages',
    'Count of messages successfully processed from topic_1',
)
prometheus_counter_topic_2_messages = Counter(
    'topic_2_messages',
    'Count of messages successfully processed from topic_2',
)
prometheus_histogram_size_messages = Histogram(
    'size_messages',
    'Histogram about messages size',
)

# From .env or default
faust_broker: str = os.environ.get('KAFKA_HOST', 'localhost:9092')

app = faust.App(
    'faust_prometheus',
    broker=faust_broker,
    env_prefix='APP_FAUST_',
    web_port=8000,
)

exporter = FaustPrometheusExporter(app)


topic_1 = app.topic('topic_1')
topic_2 = app.topic('topic_2')


@app.agent(topic_1)
async def agent_topic_1(messages: faust.Stream[str]):
    async for message in messages:
        logger.info(f'Received topic_1 message: {message}')
        prometheus_counter_topic_1_messages.inc()
        prometheus_histogram_size_messages.observe(len(message))


@app.agent(topic_2)
async def agent_topic_2(messages: faust.Stream[str]):
    async for message in messages:
        logger.info(f'Received topic_2 message: {message}')
        prometheus_counter_topic_2_messages.inc()
        prometheus_histogram_size_messages.observe(len(message))


@app.timer(interval=2.0)
async def example_sender(app):
    await topic_1.send(value=get_sentence(1))


@app.timer(interval=10.0)
async def example_sender(app):
    await topic_2.send(value=get_sentence(5))


if __name__ == '__main__':
    app.main()
