import pytest
from prometheus_client.metrics_core import Metric

from faust_prometheus.collectors import FaustCollector


class TestFaustCollector(object):
    @pytest.fixture()
    def collector(self):
        from test_app import app

        return FaustCollector(app)

    def test_init(self, collector: FaustCollector) -> None:
        assert isinstance(collector, FaustCollector)

    def test_collect(self, collector: FaustCollector) -> None:
        for metric in collector.collect():
            assert isinstance(metric, Metric)
